﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class SplashReady : MonoBehaviour
{
    public VideoPlayer player;
    public RenderTexture renderTex;
    public UnityEvent OnVideoReady;
    private bool started = false;

    public void Awake()
    {
        renderTex.Release();
    }

    public void Update()
    {
        if (player.isPrepared && !started)
        {
            started = true;
            OnVideoReady?.Invoke();
        }
    }

}
