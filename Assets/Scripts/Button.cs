﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour
{
    [HideInInspector]
    public bool pressed = false;
    public SpriteRenderer rend;
    public Sprite pressedSprite;
    [HideInInspector]
    public UnityAction OnButtonPressed;

    public void PressButton()
    {
        if (pressed) return;

        pressed = true;
        rend.sprite = pressedSprite;
        OnButtonPressed?.Invoke();
    }
}
