﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rbody;

    private float maxSpeed = 0.3f;

    private float currentXvel = 0f;
    private float currentYvel = 0f;

    public float accelRate = 0.75f;
    public float deaccelRate = 0.075f;

    public float duration = 1.5f;

    private Coroutine effectCoroutine;

    private List<SpellEffect> currentEffects;

    public Animator anim;

    private bool grounded = true;
    [HideInInspector]
    public bool canControl = true;
    private bool isStone = false;
    private bool overPit = false;
    private bool attachedToJenny = false;

    private MovingPlatform currentPlat = null;
    private Button currentButton = null;

    private Vector2 spawnPoint;

    public GameObject poofPart;

    [Header("Audio Clips")]
    public AudioSource aSource;
    public AudioClip falling;
    public AudioClip turnStone;
    public AudioClip turnNormal;
    public AudioClip stoneThud;
    public AudioSource windSource;

    [Header("Masses")]
    public float normalMass = 1f;
    public float stoneMass = 5f;

    [HideInInspector]
    private int lastRespawn = 0;

    public UnityEvent OnJennySaved;

    public void Awake()
    {
        currentEffects = new List<SpellEffect>();
        Respawn();
    }

    #region Movement
    public void Update()
    {
        //Acceleration
        if (currentEffects.Contains(SpellEffect.MOVE_RIGHT))
        {
            currentXvel = currentXvel + (accelRate * Time.deltaTime);
        }
        else if (currentXvel > 0)
        {
            currentXvel = currentXvel - (deaccelRate * Time.deltaTime);
        }

        if (currentEffects.Contains(SpellEffect.MOVE_LEFT))
        {
            currentXvel = currentXvel - (accelRate * Time.deltaTime);
        }
        else if (currentXvel < 0)
        {
            currentXvel = currentXvel + (deaccelRate * Time.deltaTime);
        }

        if (currentEffects.Contains(SpellEffect.MOVE_UP))
        {
            currentYvel = currentYvel + (accelRate * Time.deltaTime);
        }
        else if (currentYvel > 0)
        {
            currentYvel = currentYvel - (deaccelRate * Time.deltaTime);
        }

        if (currentEffects.Contains(SpellEffect.MOVE_DOWN))
        {
            currentYvel = currentYvel - (accelRate * Time.deltaTime);
        }
        else if (currentYvel < 0)
        {
            currentYvel = currentYvel + (deaccelRate * Time.deltaTime);
        }

        currentXvel = Mathf.Clamp(currentXvel, -maxSpeed, maxSpeed);
        currentYvel = Mathf.Clamp(currentYvel, -maxSpeed, maxSpeed);

        float medianSpeed = (Mathf.Abs(currentXvel) + Mathf.Abs(currentYvel)) / 2;

        ///Calculates grounded from median also.
        grounded = medianSpeed < 0.01f;

        if (!grounded && !attachedToJenny)
        {
            rbody.AddForce(new Vector2(currentXvel, currentYvel) * 10);
            if (transform.parent != null) transform.SetParent(null);
        }

        anim.SetFloat("Speed", medianSpeed);

        CheckGround();

        UpdateWindVolume(medianSpeed);
    }

    private void CheckGround()
    {
        if (!grounded) return;

        if (currentPlat != null)
        {
            if (transform.parent != currentPlat.transform)
            {
                transform.SetParent(currentPlat.transform);
                EraseVelocities();
            }
        }
        else
        {
            if (overPit)
            {
                Fall();
            }
        }
    }

    private void UpdateWindVolume(float median)
    {
        windSource.volume = isStone ? 0f : Mathf.InverseLerp(0f, maxSpeed, median);
    }

    private void ResetLinkedObjs()
    {
        currentPlat = null;
        transform.SetParent(null);
        currentButton = null;
    }

    public void MovePlayer(SpellEffect dir)
    {
        if (!canControl) return;

        if (!currentEffects.Contains(dir)) effectCoroutine = StartCoroutine(MoveCoroutine(dir));
    }

    private IEnumerator MoveCoroutine(SpellEffect dir)
    {
        currentEffects.Add(dir);

        yield return new WaitForSeconds(duration);

        currentEffects.Remove(dir);
    }

    private void EraseVelocities()
    {
        //Resets velocities and anims
        currentXvel = 0f;
        currentYvel = 0f;

        rbody.velocity = Vector2.zero;
    }
    #endregion

    #region Spell Controlling
    public void TurnStone()
    {
        if (!canControl) return;

        if (!isStone)
        {
            canControl = false;
            anim.SetTrigger("Stone");
            EraseVelocities();
            aSource.PlayOneShot(turnStone);
        }
    }

    public void SetToStone()
    {
        isStone = true;
        rbody.mass = stoneMass;
        canControl = true;
    }

    public void BackNormalSound()
    {
        aSource.PlayOneShot(turnNormal);
    }

    public void SetBackToNormal()
    {
        isStone = false;
        rbody.mass = normalMass;
    }

    internal void CancelSpell()
    {
        if (isStone)
        {
            anim.SetTrigger("BackToNormal");
        }

        if (attachedToJenny) OnJennySaved?.Invoke();
    }
    #endregion

    public void ScreenShakeStone()
    {
        Camera.main.DOShakePosition(0.5f, 0.5f, 20);
        poofPart.SetActive(true);
        aSource.PlayOneShot(stoneThud);

        if (currentButton != null)
        {
            currentButton.PressButton();
            currentButton = null;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Pit" && collision.OverlapPoint(transform.position) && currentPlat == null)
        {
            overPit = true;
        }
        else
        {
            overPit = false;
        }

        if (collision.tag == "Windzone" && !isStone)
        {
            if (grounded) grounded = false;

            Vector2 wind = collision.GetComponent<Windzone>().windForce;

            currentXvel += wind.x * Time.deltaTime;
            currentYvel += wind.y * Time.deltaTime;
        }
    }

    private void Fall()
    {
        if (grounded && canControl)
        {
            canControl = false;
            anim.SetTrigger("Fall");
            aSource.PlayOneShot(falling);
            EraseVelocities();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Spawn")
        {
            UpdateSpawnPoint(collision.GetComponent<SpawnPoint>());
        }

        if (collision.tag == "Button")
        {
            currentButton = collision.GetComponent<Button>();
        }

        if (collision.tag == "MovingPlatform")
        {
            currentPlat = collision.GetComponent<MovingPlatform>();
        }

        if (collision.tag == "Jenny")
        {
            AttachToJenny(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Button" && currentButton != null)
        {
            currentButton = null;
        }

        if (collision.tag == "MovingPlatform")
        {
            currentPlat = null;
        }
    }

    public void Respawn()
    {
        EraseVelocities();
        SetBackToNormal();
        ResetLinkedObjs();

        canControl = true;
        isStone = false;
        overPit = false;

        anim.SetTrigger("Idle");

        transform.position = spawnPoint;
    }

    public void UpdateSpawnPoint(SpawnPoint sp)
    {
        if (lastRespawn >= sp.ID) return;

        lastRespawn = sp.ID;
        spawnPoint = sp.transform.position;

        sp.Activate();
    }


    private void AttachToJenny(GameObject jen)
    {
        EraseVelocities();
        canControl = false;
        attachedToJenny = true;

        Camera.main.transform.DOMove(new Vector3(jen.transform.position.x, jen.transform.position.y, -10), 1f).SetEase(Ease.OutExpo);

        transform.DOMove(jen.transform.position, 1f).SetEase(Ease.OutBack).OnComplete(() => canControl = true);
    }
}
