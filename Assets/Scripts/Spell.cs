﻿using System;
using System.Collections.Generic;

[Serializable]
public class Spell
{
    public string name;
    public string desc;

    public SpellEffect effect;

    public List<int> positions;
}

public enum SpellEffect
{
    MOVE_RIGHT,
    MOVE_LEFT,
    MOVE_UP,
    MOVE_DOWN,
    STONE,
    CANCEL
}