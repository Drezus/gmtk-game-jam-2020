﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class MoveCamera : MonoBehaviour
{
    public PlayerMovement player;
    public float yDist = 15f;
    public float xDist = 35f;
    public float duration = 1f;
    private bool lerping = false;


    public void LerpCam(Directions dir)
    {
        lerping = true;

        switch (dir)
        {
            case Directions.UP:
                transform.DOMoveY(transform.position.y + yDist, duration).SetEase(Ease.OutExpo).OnComplete(() => OnLerpFinished());
                break;
            case Directions.DOWN:
                transform.DOMoveY(transform.position.y - yDist, duration).SetEase(Ease.OutExpo).OnComplete(() => OnLerpFinished());
                break;
            case Directions.LEFT:
                transform.DOMoveX(transform.position.x - xDist, duration).SetEase(Ease.OutExpo).OnComplete(() => OnLerpFinished());
                break;
            case Directions.RIGHT:
                transform.DOMoveX(transform.position.x + xDist, duration).SetEase(Ease.OutExpo).OnComplete(() => OnLerpFinished());
                break;
        }
    }

    private void OnLerpFinished()
    {
        lerping = false;
    }

    private void Update()
    {
        CheckPlayerPos();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    /// <summary>
    /// Checks if the player is beyond the camera's bounds on any direction, and lerps the camera towards it to keep it on-screen.
    /// </summary>
    private void CheckPlayerPos()
    {
        if (lerping) return;

        if (player.transform.position.y >= transform.position.y + (yDist / 2))
        {
            LerpCam(Directions.UP);
            return;
        }

        if (player.transform.position.y <= transform.position.y - (yDist / 2))
        {
            LerpCam(Directions.DOWN);
            return;
        }

        if (player.transform.position.x >= transform.position.x + (xDist / 2))
        {
            LerpCam(Directions.RIGHT);
            return;
        }

        if (player.transform.position.x <= transform.position.x - (xDist / 2))
        {
            LerpCam(Directions.LEFT);
            return;
        }
    }
}

public enum Directions
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}