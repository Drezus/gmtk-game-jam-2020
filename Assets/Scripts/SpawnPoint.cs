﻿using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public int ID;
    public Animator anim;
    public GameObject particle;
    public AudioSource aSource;


    public void Activate()
    {
        anim.SetTrigger("Activate");
        particle.SetActive(true);
        aSource.Play();
    }
}