﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Collider2D coll;
    public SpriteRenderer rend;
    public AudioSource aSource;
    public List<Button> requiredButtons;

    private void Start()
    {
        if (requiredButtons == null || requiredButtons.Count <= 0) UnlockDoor();

        foreach (Button bu in requiredButtons)
        {
            bu.OnButtonPressed += CheckDoorState;
        }
    }

    private void CheckDoorState()
    {
        foreach (Button bu in requiredButtons)
        {
            if (!bu.pressed) return;
        }

        UnlockDoor();
    }

    private void UnlockDoor()
    {
        rend.enabled = false;
        coll.enabled = false;
        aSource.Play();
    }
}
