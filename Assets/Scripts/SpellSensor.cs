﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpellSensor : EventTrigger
{
    [HideInInspector]
    public bool traced = false;

    public override void OnPointerEnter(PointerEventData data)
    {
        Trace();
    }
    public override void OnPointerDown(PointerEventData data)
    {
        Trace();
    }

    private void Trace()
    {
        if (Input.GetMouseButton(0) && !traced)
        {
            traced = true;
            GetComponent<Image>().color = Color.cyan;
        }
    }

    public void ResetSensor()
    {
        traced = false;
        GetComponent<Image>().color = Color.white;
    }

}
