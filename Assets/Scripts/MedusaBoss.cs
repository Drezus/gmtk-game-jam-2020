﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedusaBoss : MonoBehaviour
{
    public PlayerMovement player;
    public GameObject rightWind, leftWind;
    public float attackDelay, attackDuration;
    public AudioSource aSource;

    public Transform medusaSprite;
    private Directions direction;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DelayedAttack());
    }

    private IEnumerator DelayedAttack()
    {
        yield return new WaitForSeconds(attackDelay);
        aSource.Play();
        if (direction == Directions.RIGHT)
        {
            rightWind.SetActive(true);
        }
        else if (direction == Directions.LEFT)
        {
            leftWind.SetActive(true);
        }

        yield return new WaitForSeconds(attackDuration);

        leftWind.SetActive(false);
        rightWind.SetActive(false);

        StartCoroutine(DelayedAttack());
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x > transform.position.x && direction != Directions.RIGHT)
        {
            direction = Directions.RIGHT;
            medusaSprite.localScale = new Vector2(1f, 1f);

        }
        else if (player.transform.position.x < transform.position.x && direction != Directions.LEFT)
        {
            direction = Directions.LEFT;
            medusaSprite.localScale = new Vector2(-1f, 1f);
        }
    }
}
