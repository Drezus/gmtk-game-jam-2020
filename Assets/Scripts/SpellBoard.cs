﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBoard : MonoBehaviour
{
    public List<SpellSensor> sensors;

    public List<Spell> spells;

    private bool tracing = false;

    public void Update()
    {
        if (Input.GetMouseButtonDown(0) && !tracing)
        {
            tracing = true;
        }

        if (Input.GetMouseButtonUp(0) && tracing)
        {
            tracing = false;
            CheckSpell();
        }
    }

    private void CheckSpell()
    {
        List<int> castedSpots = new List<int>();

        for (int i = 0; i < sensors.Count; i++)
        {
            if (sensors[i].traced) castedSpots.Add(i);
        }

        IdentifySpell(castedSpots);
    }

    private void IdentifySpell(List<int> castedSpots)
    {
        foreach (Spell spe in spells)
        {
            if (ListMatch(castedSpots, spe.positions))
            {
                CastSpell(spe);
                return;
            }
        }

        NoSpellCast();
    }

    private void CastSpell(Spell spe)
    {
        switch (spe.effect)
        {
            case SpellEffect.MOVE_LEFT:
            case SpellEffect.MOVE_RIGHT:
            case SpellEffect.MOVE_UP:
            case SpellEffect.MOVE_DOWN:
                FindObjectOfType<PlayerMovement>().MovePlayer(spe.effect);
                break;
            case SpellEffect.STONE:
                FindObjectOfType<PlayerMovement>().TurnStone();
                break;
            case SpellEffect.CANCEL:
                FindObjectOfType<PlayerMovement>().CancelSpell();
                break;
        }


        ResetBoard();
    }

    private void NoSpellCast()
    {
        Debug.Log("No spell found.");
        ResetBoard();
    }

    private void ResetBoard()
    {
        foreach (SpellSensor sen in sensors)
        {
            sen.ResetSensor();
        }
    }


    private bool ListMatch(List<int> listA, List<int> listB)
    {
        if (listA.Count != listB.Count) return false;

        listA.Sort();
        listB.Sort();

        for (int i = 0; i < listA.Count; i++)
        {
            if (listA[i] != listB[i]) return false;
        }

        return true;
    }

}

