﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Directions currentDirection;
    public float speed = 0.01f;
    private Vector2 currentVel = Vector2.zero;

    public Vector2 limits;

    public void Start()
    {
        SwitchDirection(currentDirection);
    }

    private void SwitchDirection(Directions direction)
    {
        currentDirection = direction;

        switch (direction)
        {
            case Directions.UP:
                currentVel = new Vector2(0f, speed);
                break;
            case Directions.DOWN:
                currentVel = new Vector2(0f, -speed);
                break;
            case Directions.RIGHT:
                currentVel = new Vector2(speed, 0f);
                break;
            case Directions.LEFT:
                currentVel = new Vector2(-speed, 0f);
                break;
        }
    }

    public void Update()
    {
        transform.Translate(currentVel);
        CheckBounds();
    }

    private void CheckBounds()
    {
        //Change direction when reaching limits
        if (transform.position.y >= limits.x && currentDirection == Directions.UP)
        {
            SwitchDirection(Directions.DOWN);
        }
        else if (transform.position.y <= limits.y && currentDirection == Directions.DOWN)
        {
            SwitchDirection(Directions.UP);
        }

        if (transform.position.x >= limits.x && currentDirection == Directions.RIGHT)
        {
            SwitchDirection(Directions.LEFT);
        }
        else if (transform.position.x <= limits.y && currentDirection == Directions.LEFT)
        {
            SwitchDirection(Directions.RIGHT);
        }
    }
}
