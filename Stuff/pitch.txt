GMTK GAME JAM 2020
- OUT OF CONTROL - 

HAT TRICK
by Andr� "Drezus" Segato
Special thanks to Caique "Barsil" Barsil.

Free sounds from Zapsplat and freesoundeffects.com

The Evil Witch turned the clumsy apprentice into a stone statue! But her spirit and knowledge lives on... inside her trusty witch hat!
Use spells to move the hat around, explore the Hatnomicon to learn new spells and their rune patterns, clear obstacle, fight foes and drift your way to defeat the hag!


TO-DO:
Basic level design:
-Pits [OK]
-Recover/spawn points [OK]
-Camera change between rooms [OK]
-Moving Platforms [OK]
-Stone power [OK]
-Fire power [X]
-Respawn sensors [OK]
-Press Switches [OK]
-Windzones[OK]
-Collect keys [X]
-Evil Witch fight [X] [IN RETROSPECT LETS DO THIS] [OK!!!!!!!!!!!!!]
-Jenny [OK]

(Possible)
-Electric power [X]
-Wall drag [X]

Polish:
-Intro cinematic
--Ending?
-Book interface
-Saving/Loading [Sorry but X]

ART:
-Medusa (3 poses)
-Jenny (1 pose, stone)
-Hat (cutscene)
